# generator-frnt [![Build Status](https://secure.travis-ci.org/nathanaelphilip/generator-frnt.png?branch=master)](https://travis-ci.org/nathanaelphilip/generator-frnt)

> [Yeoman](http://yeoman.io) generator


## FRNT

A quick start up generator for Stitch web projects.
