<!DOCTYPE html>
<html>
<head>
    <title><%= title %></title>
    <!-- meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <!-- css -->
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- fonts -->
    <!-- scripts -->
    <!--[if IE]><script src="assets/js/vendor/html5shiv.js"></script><![endif]-->
    <script src="assets/js/vendor/modernizr.js"></script>
</head>
<body>
	<script src="assets/js/vendor/jquery.js"></script>
	<script src="assets/js/app.min.js"></script>
</body>
</html>